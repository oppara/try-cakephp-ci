<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{

    public $import = ['table' => 'users'];

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'email' => 'foo@example.com',
                'password' => 'passw0rd',
                'created' => '2020-10-24 22:22:07',
                'modified' => '2020-10-24 22:22:07',
            ],
        ];
        parent::init();
    }
}
