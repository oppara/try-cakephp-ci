# try-cakephp-ci 

Gitlab CI で CakePHP のテストを実行してみる。


## カバレッジの設定

Settings -> CI/CD -> General pipelines -> Test coverage parsing

```
^\s*Lines:\s*\d+.\d+\%
```

## バッジの設定

Settings -> General -> Badges 

### Pipeline status

- Name: pipeline
- Link: https://gitlab.e-2.jp/%{project_path}
- Badge image URL: https://gitlab.e-2.jp//%{project_path}/badges/%{default_branch}/pipeline.svg

### Coverage report

- Name: coverage
- Link: https://gitlab.e-2.jp/%{project_path}/-/commits/%{default_branch}
- Badge image URL: https://gitlab.e-2.jp/%{project_path}/badges/%{default_branch}/coverage.svg


